package com.example.weixinapireptile.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.weixinapireptile.pojo.entity.User;
import com.example.weixinapireptile.pojo.entity.WxArticle;
import com.example.weixinapireptile.pojo.query.WxArticleQuery;
import com.example.weixinapireptile.pojo.vo.WxArticleVO;
import io.swagger.annotations.ApiParam;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author xunuo
 * @description: TODO
 * @datetime 2023年 11月 03日 10:57
 */
public interface IUserLoginService {

     Map<String, Object> login(String username, String password);

     int updatePassword(User user);

     void logout();
}
