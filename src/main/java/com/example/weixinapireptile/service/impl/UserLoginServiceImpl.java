package com.example.weixinapireptile.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.example.weixinapireptile.api.RedisCache;
import com.example.weixinapireptile.common.exceptions.BizException;
import com.example.weixinapireptile.common.seecurity.LoginUser;
import com.example.weixinapireptile.common.seecurity.model.JwtProperties;
import com.example.weixinapireptile.common.utils.JwtUtils;
import com.example.weixinapireptile.mapper.UserMapper;
import com.example.weixinapireptile.pojo.entity.User;
import com.example.weixinapireptile.service.IUserLoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author xunuo
 * @description: TODO
 * @datetime 2023年 12月 28日 17:02
 */
@Slf4j
@Service
public class UserLoginServiceImpl implements IUserLoginService {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private RedisCache redisCache;
    @Autowired
    private UserMapper userMapper;

    @Override
    public Map<String, Object> login(String username, String password) {
        //使用AuthenticationManager进行用户认证
        //先将用户名和密码封装进authentication对象。
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        //封装完传进来继续调用下一层进行认证。
        try {
            Authentication authenticate = authenticationManager.authenticate(authenticationToken);
            //认证没通过，给出相应提示
            if (Objects.isNull(authenticate)) {
                BizException.throwe("认证失败");
            }
            //认证通过，用jwt生成token,将其直接返回。
            LoginUser loginUser = (LoginUser) authenticate.getPrincipal();
            String userId = loginUser.getUser().getId();
            Map<String, Object> claims = new HashMap<>();
            claims.put("id", userId);
            String token = JwtUtils.createJWT(
                    JwtProperties.Key.SECRET_KEY,
                    JwtProperties.Config.TTL,
                    claims);
            //将完整的用户信息存入redis, id作为key
            updateCache(userId, loginUser.getUser());
            //返回token给前端
            Map<String, Object> result = new HashMap<>();
            loginUser.setPassword(null);
            result.put("token", token);
            result.put("loginUser", loginUser);
            return result;
        } catch (Exception e) {
            log.error("登录异常：" + e.getMessage());
            BizException.throwe(e.getMessage());
        }
        return null;
    }

    @Override
    public int updatePassword(User user) {
        if (user==null||user.getUserName()==null || user.getPassword()==null){
            BizException.throwe("请正确输入密码！");
        }
        if ("admin".equals(user.getUserName())&& !"AuthorizationCode".equals(user.getAuthorizationCode())){
            BizException.throwe("授权码错误，管理员密码不可修改！");
        }
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        //加密
        String encodePassword = bCryptPasswordEncoder.encode(user.getPassword());
        //更新密码
        return userMapper.update(new LambdaUpdateWrapper<User>().eq(User::getUserName, user.getUserName()).set(User::getPassword, encodePassword));
    }

    @Override
    public void logout(){
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                = (UsernamePasswordAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
        User loginUser = (User) usernamePasswordAuthenticationToken.getPrincipal();
        String userId = loginUser.getId();
        String key = "login::" + userId;
        redisCache.deleteObject(key);
    }

    /**
     * 插入和更新缓存数据
     *
     * @param
     */
    private void updateCache(String id, User loginUser) {
        String key = "login::" + id;
        redisCache.setCacheObject(key, loginUser);
    }
}
