package com.example.weixinapireptile.controller;

import com.example.weixinapireptile.common.result.Result;
import com.example.weixinapireptile.common.seecurity.LoginUser;
import com.example.weixinapireptile.common.seecurity.SecurityUtils;
import com.example.weixinapireptile.service.IUserLoginService;
import com.example.weixinapireptile.pojo.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Map;

/**
 * @author xunuo
 * @description: TODO
 * @datetime 2023年 12月 28日 16:59
 */
@Api(tags = "登录API")
@RestController
@RequestMapping("/user")
public class SysUserController {

    @Autowired
    IUserLoginService userLoginService;

    @ApiOperation("登录")
    @PostMapping("/login")
    public Result<Map<String, Object>> login(@RequestBody User user){
        //登录
        Map<String, Object> login = userLoginService.login(user.getUserName(),user.getPassword());
        return Result.success(login);
    }
    @ApiOperation("登出")
    @GetMapping("/logout")
    public Result<Map<String, Object>> logout(){
        //登出
        userLoginService.logout();
        return Result.success();
    }

    @ApiOperation("修改密码")
    @PostMapping("/updatePassword")
    public Result<Map<String, Object>> updatePassword(@RequestBody User user){
        int i = userLoginService.updatePassword(user);
        if (i <= 0){
            return Result.failed("修改失败！");
        }
        return Result.success();
    }

    @ApiOperation("获取登录信息")
    @GetMapping("/getUserInfo")
    public Result<User> getUserInfo(){
        return Result.success(SecurityUtils.getUserInfo());
    }

}
