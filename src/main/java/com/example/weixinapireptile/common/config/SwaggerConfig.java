package com.example.weixinapireptile.common.config;

import com.example.weixinapireptile.common.seecurity.model.JwtProperties;
import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import java.util.List;

/**
 * @author xunuo
 * @description: TODO
 * @datetime 2023年 11月 10日 16:17
 */

@Configuration
@EnableSwagger2WebMvc
@EnableKnife4j
public class SwaggerConfig {
    @Bean
    public Docket docket() {

        ParameterBuilder parameterBuilder = new ParameterBuilder();
        List<Parameter> parameters = Lists.newArrayList();
        parameterBuilder
                .name(JwtProperties.Header.TOKEN_NAME).description("认证令牌")
                .modelRef(new ModelRef("String"))
                .parameterType("header")
                .order(-99)
                .required(false).build();
        parameters.add(parameterBuilder.build());

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any()).build().globalOperationParameters(parameters);

    }


    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("swagger和springBoot整合").description("swagger的API文档")
                .version("1.0").build();
    }

}
