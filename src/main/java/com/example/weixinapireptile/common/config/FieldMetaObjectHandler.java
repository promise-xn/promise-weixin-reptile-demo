package com.example.weixinapireptile.common.config;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.example.weixinapireptile.common.seecurity.SecurityUtils;
import org.apache.ibatis.reflection.MetaObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 数据库字段默认填充
 *
 * @author zhanglincheng
 * @date 2022/10/1
 */
@Component
public class FieldMetaObjectHandler implements MetaObjectHandler {
    private final static Logger log = LoggerFactory.getLogger(FieldMetaObjectHandler.class);

    @Override
    public void insertFill(MetaObject metaObject) {
        Date now = new Date();
        this.strictInsertFill(metaObject, "createTime", Date.class, now);
        this.strictInsertFill(metaObject, "updateTime", Date.class, now);
        String userId = SecurityUtils.getUserId();
        if (StrUtil.isNotEmpty(userId)) {
            this.setFieldValByName("createBy", userId, metaObject);
            this.setFieldValByName("updateBy", userId, metaObject);
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictUpdateFill(metaObject, "updateTime", Date.class, new Date());
        String userId = SecurityUtils.getUserId();
        if (StrUtil.isNotEmpty(userId)) {
            this.setFieldValByName("updateBy", userId, metaObject);
        }
    }
}
