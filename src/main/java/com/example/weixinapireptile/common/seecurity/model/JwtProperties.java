package com.example.weixinapireptile.common.seecurity.model;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author xunuo
 * @description: TODO
 * @datetime 2023年 12月 28日 16:06
 */
@Component
@Data
public class JwtProperties {

    public static class Key{
        /**
         * 用户生成jwt秘钥
         */
        public  static final String SECRET_KEY = "853b5214060f4cd5a8b022aa717eaba9";
    }

    public static class Header{
        /**
         * 请求头名称
         */
        public  static final String TOKEN_NAME = "token";
    }

    public static class Config{
        /**
         * 用户过期时间
         */
        public  static final long TTL = 7200000;
    }




}
