package com.example.weixinapireptile.common.seecurity;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.weixinapireptile.common.exceptions.BizException;
import com.example.weixinapireptile.mapper.UserMapper;
import com.example.weixinapireptile.pojo.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author xunuo
 * @description: TODO
 * @datetime 2023年 12月 28日 16:13
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LambdaQueryWrapper<User> eq = new LambdaQueryWrapper<User>().eq(User::getUserName, username);
        User user = userMapper.selectOne(eq);

        if (ObjectUtil.isEmpty(user)){
            throw new BizException("用户不存在！");
        }
        return new LoginUser(user);
    }
}
