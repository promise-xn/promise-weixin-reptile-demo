package com.example.weixinapireptile.common.seecurity;

import com.example.weixinapireptile.pojo.entity.User;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author xunuo
 * @description: TODO
 * @datetime 2024年 01月 02日 16:06
 */
public class SecurityUtils {

    public static User getUserInfo(){
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public static String getUsername(){
        return getUserInfo().getUserName();
    }
    public static String getUserId(){
        return getUserInfo().getId();
    }
}
