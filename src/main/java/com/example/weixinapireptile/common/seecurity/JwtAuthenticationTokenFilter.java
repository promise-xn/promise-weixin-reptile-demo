package com.example.weixinapireptile.common.seecurity;

import com.example.weixinapireptile.api.RedisCache;
import com.example.weixinapireptile.common.exceptions.BizException;
import com.example.weixinapireptile.common.result.HttpServletResult;
import com.example.weixinapireptile.common.result.ResultStatus;
import com.example.weixinapireptile.common.seecurity.model.JwtProperties;
import com.example.weixinapireptile.common.utils.JwtUtils;
import com.example.weixinapireptile.pojo.entity.User;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

import static javax.xml.bind.JAXBIntrospector.getValue;

/**
 * @author xunuo
 * @description: TODO
 * @datetime 2023年 12月 29日 15:50
 */
@Component
@Slf4j
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    @Autowired
    RedisCache redisCache;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException{
        //获取token
        String token = request.getHeader(JwtProperties.Header.TOKEN_NAME);
        if(!StringUtils.hasText(token)){
            //放行
            filterChain.doFilter(request,response);//可能是登录也可能不是，需要放行后交给后面去判断。
//            HttpServletResult.result(response, ResultStatus.TOKEN_ACCESS_FORBIDDEN);
            return ; //这里是为了获取结果返回的时候不往下执行。
        }
        // 解析token
        String id = null;
        try {
            Claims claims = JwtUtils.parseJWT(JwtProperties.Key.SECRET_KEY,token);
            id = claims.get("id").toString();
            log.info("用户id为:{}",id);
        } catch (Exception e) {
            BizException.throwe("用户未登录！");
        }
        //从redis获取认证用户信息。
        String redisKey="login::"+id;
        User loginUser=(User) getValue(redisKey);
        log.info("从redis获取用户信息为:{}",loginUser);
        if(Objects.isNull(loginUser)){
            BizException.throwe("用户未登录！");
        }
        //存入SecurityContextHolder
        //TODO 获取权限信息封装到authentication中
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(loginUser,null,null);
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        //放行
        filterChain.doFilter(request,response);
    }

    private Object getValue(String key){
        return  redisCache.getCacheObject(key);
    }
}
