package com.example.weixinapireptile.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.weixinapireptile.common.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author xunuo
 * @description: TODO
 * @datetime 2023年 12月 28日 16:09
 */

@Data
@TableName(value = "sys_user")
public class User extends BaseEntity {
    @TableField("user_name")
    private String userName;
    @TableField("password")
    private String password;
    @TableField(exist = false)
    private String authorizationCode;
}
