# reptile-wx-vue

## 介绍

使用vue2.0 + vue-router + vuex + axios + element-ui 构建

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
