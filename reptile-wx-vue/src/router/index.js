import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginView from '../views/LoginView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    component: LoginView
  },
  {
    path: '/admin',
    name: 'adminHome',
    redirect:'/admin/home',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AdminHomeView.vue'),
    children:[
      {
        path:'home',
        component:()=>import('../views/admin/home/HomeView'),
        meta:{requireAuth:true}
      },
      {
        path:'category',
        component:()=>import('../views/admin/category/CategoryView'),
        meta:{requireAuth:true}
      },
      {
        path:'news',
        component:()=>import('../views/admin/news/NewsView'),
        meta:{requireAuth:true}

      },
      {
        path:'newsDetail',
        component:()=>import('../views/admin/news/NewsDetailView'),
        meta:{requireAuth:true}
      }
    ]
  },
  {
    path: '*',
    component:  () => import('../views/error/Error404View')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
