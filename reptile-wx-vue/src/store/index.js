import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: null, // 初始化token为null
  },
  getters: {
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
      localStorage.setItem('token', token);
    },
  },
  actions: {
  },
  modules: {
  }

})
// const store = new Vuex.Store({
//   state: {
//     token: null, // 初始化token为null
//   },
//   mutations: {
//     setToken(state, token) {
//       state.token = token;
//     },
//   },
// });
