import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// main.js
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from 'axios'

Vue.use(ElementUI);
Vue.prototype.axios = axios

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  if (to.meta.requireAuth) { // 判断该路由是否需要登录权限
    if(localStorage.getItem('token')){ //判断本地是否存在token
      next();
    }else {
      if(to.path === '/'){
        next();
      }else {
        next({
          path:'/'
        })
      }
    }
  }
  else {
    next();
  }
  /*如果本地 存在 token 则 不允许直接跳转到 登录页面*/
  if(to.fullPath == "/"){
    if(localStorage.getItem('userInfo')){
      alert('您已经登录了，如果想要登录其他账号，请先退出当前账号！')
      next({
        path:from.fullPath
      });
    }else {
      next();
    }
  }
});

// 配置axios的请求拦截器
axios.interceptors.request.use(config => {
  const isLoginRequest = config.url.endsWith('/login') || config.url.includes('/login/') || config.url.endsWith("/updatePassword"); // 根据实际登录接口URL进行调整
  // const token = localStorage.getItem('token') || store.state.token;
  const token = localStorage.getItem('token')
  if (!isLoginRequest && token) {
    // 如果不是登录请求，并且存在token，则添加到请求头中
    config.headers['token'] = `${token}`;
  } else {
    // 如果是登录请求或者没有token，则不移除或添加token到headers中
    // 这里也可以删除headers中可能已经存在的token，以确保登录请求不携带token
    delete config.headers['token'];
  }

  return config;
}, error => {
  // 处理请求错误
  return Promise.reject(error);
});

// 配置axios的响应拦截器
axios.interceptors.response.use(response => {
  // 对响应数据做点什么
  return response;
}, error => {
  if (error.response) {
    // 请求已发出，服务器也响应了状态码，但状态码不在 2xx 范围内
    switch (error.response.status) {
      case 401: // 未授权，通常表示token无效或未提供
      case 403: // 禁止访问，通常也表示认证失败
        // 清除token（如果需要）
        localStorage.removeItem('token'); // 或者使用 store.commit('clearToken')
        // 跳转到登录页面
        router.push('/'); // 假设登录页面的路由是'/login'
        break;
        // 其他状态码的处理...
    }
  } else if (error.request) {
    // 请求已发出，但没有收到任何响应
    // 这通常发生在网络错误或请求被取消时
  } else {
    // 发送请求时发生了一些问题
  }
  // 返回reject的promise
  return Promise.reject(error);
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
